### Run rabbitMQ using docker
 - `docker-compose up -d`

### Run the app using command line
 - make build

 - Run as publisher `./out/go-rabbit publish` in another terminal

 - Run as publisher `./out/go-rabbit subscribe` in another terminal
